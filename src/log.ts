import 'log-timestamp'
import fetch from 'node-fetch'

var logApiUrl = process.env.LOG_API_URL || (
  `https://alpha.curatiojournal.com/api/log`
);
logApiUrl = logApiUrl.replace(/\/$/, '');

if ((process.env.X_API || "").length < 2) {
  throw new Error("[log module] X_API env var not set");
}

export async function log(message: any | any[], level: LogLevel = 'info', opts: LogOptions = {}) {
  if (['info', 'debug', 'warning', 'error'].indexOf(level) === -1) {
    level = 'info'
    log(["Unknown log level:", level], 'warning', { remote: true });
  }

  var fn: any = {
    "info": console.info,
    "warning": console.warn,
    "error": console.error,
    "debug": console.debug
  }[level];

  if (typeof fn === "function") {
    fn(...(!Array.isArray(message) ? [message] : message));
  }

  if (Array.isArray(message)) {
    for (let i = 0, n = message.length; i < n; i++) {
      message[i] = objectifyError(message[i]);
    }
  } else {
    message = objectifyError(message);
  }

  if (opts.remote) {
    var xApi = process.env.X_API;

    await fetch(
      `${logApiUrl}/log`,
      {
        method: 'POST',
        headers: {
          "Content-Type": "application/json",
          "Accept": "application/json",
          "x-api": xApi
        } as any,
        body: JSON.stringify({
          message,
          severity: {
            'debug': 'debug',
            'info': 'info',
            'warning': 'warning',
            'error': 'danger'
          }[level]
        })
      }
    ).then(async r => {
      if (!/^2/.test(r.status.toString())) {
        log(['log remote invocation failed', r.status, r.statusText, await r.text()], 'error', { remote: false })
      }
    }).catch(e => {
      log(['log remote invocation failed', e], 'error', { remote: false })
    })
  }
}

export type LogLevel = 'info' | 'debug' | 'warning' | 'error';

export interface LogOptions {
  /**
   * Whether to send to the remote API.
   */
  remote?: boolean;
}

function objectifyError(e: any) {
  if (e instanceof Error) {
    return {
      ...e,
      message: e.message,
      name: e.name,
      stack: e.stack
    }
  }

  return e;
}