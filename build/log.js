"use strict";
var __assign = (this && this.__assign) || function () {
    __assign = Object.assign || function(t) {
        for (var s, i = 1, n = arguments.length; i < n; i++) {
            s = arguments[i];
            for (var p in s) if (Object.prototype.hasOwnProperty.call(s, p))
                t[p] = s[p];
        }
        return t;
    };
    return __assign.apply(this, arguments);
};
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : new P(function (resolve) { resolve(result.value); }).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __generator = (this && this.__generator) || function (thisArg, body) {
    var _ = { label: 0, sent: function() { if (t[0] & 1) throw t[1]; return t[1]; }, trys: [], ops: [] }, f, y, t, g;
    return g = { next: verb(0), "throw": verb(1), "return": verb(2) }, typeof Symbol === "function" && (g[Symbol.iterator] = function() { return this; }), g;
    function verb(n) { return function (v) { return step([n, v]); }; }
    function step(op) {
        if (f) throw new TypeError("Generator is already executing.");
        while (_) try {
            if (f = 1, y && (t = op[0] & 2 ? y["return"] : op[0] ? y["throw"] || ((t = y["return"]) && t.call(y), 0) : y.next) && !(t = t.call(y, op[1])).done) return t;
            if (y = 0, t) op = [op[0] & 2, t.value];
            switch (op[0]) {
                case 0: case 1: t = op; break;
                case 4: _.label++; return { value: op[1], done: false };
                case 5: _.label++; y = op[1]; op = [0]; continue;
                case 7: op = _.ops.pop(); _.trys.pop(); continue;
                default:
                    if (!(t = _.trys, t = t.length > 0 && t[t.length - 1]) && (op[0] === 6 || op[0] === 2)) { _ = 0; continue; }
                    if (op[0] === 3 && (!t || (op[1] > t[0] && op[1] < t[3]))) { _.label = op[1]; break; }
                    if (op[0] === 6 && _.label < t[1]) { _.label = t[1]; t = op; break; }
                    if (t && _.label < t[2]) { _.label = t[2]; _.ops.push(op); break; }
                    if (t[2]) _.ops.pop();
                    _.trys.pop(); continue;
            }
            op = body.call(thisArg, _);
        } catch (e) { op = [6, e]; y = 0; } finally { f = t = 0; }
        if (op[0] & 5) throw op[1]; return { value: op[0] ? op[1] : void 0, done: true };
    }
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
require("log-timestamp");
var node_fetch_1 = __importDefault(require("node-fetch"));
var logApiUrl = process.env.LOG_API_URL || ("https://alpha.curatiojournal.com/api/log");
logApiUrl = logApiUrl.replace(/\/$/, '');
if ((process.env.X_API || "").length < 2) {
    throw new Error("[log module] X_API env var not set");
}
function log(message, level, opts) {
    if (level === void 0) { level = 'info'; }
    if (opts === void 0) { opts = {}; }
    return __awaiter(this, void 0, void 0, function () {
        var fn, i, n, xApi;
        var _this = this;
        return __generator(this, function (_a) {
            switch (_a.label) {
                case 0:
                    if (['info', 'debug', 'warning', 'error'].indexOf(level) === -1) {
                        level = 'info';
                        log(["Unknown log level:", level], 'warning', { remote: true });
                    }
                    fn = {
                        "info": console.info,
                        "warning": console.warn,
                        "error": console.error,
                        "debug": console.debug
                    }[level];
                    if (typeof fn === "function") {
                        fn.apply(void 0, (!Array.isArray(message) ? [message] : message));
                    }
                    if (Array.isArray(message)) {
                        for (i = 0, n = message.length; i < n; i++) {
                            message[i] = objectifyError(message[i]);
                        }
                    }
                    else {
                        message = objectifyError(message);
                    }
                    if (!opts.remote) return [3 /*break*/, 2];
                    xApi = process.env.X_API;
                    return [4 /*yield*/, node_fetch_1.default(logApiUrl + "/log", {
                            method: 'POST',
                            headers: {
                                "Content-Type": "application/json",
                                "Accept": "application/json",
                                "x-api": xApi
                            },
                            body: JSON.stringify({
                                message: message,
                                severity: {
                                    'debug': 'debug',
                                    'info': 'info',
                                    'warning': 'warning',
                                    'error': 'danger'
                                }[level]
                            })
                        }).then(function (r) { return __awaiter(_this, void 0, void 0, function () {
                            var _a, _b;
                            return __generator(this, function (_c) {
                                switch (_c.label) {
                                    case 0:
                                        if (!!/^2/.test(r.status.toString())) return [3 /*break*/, 2];
                                        _a = log;
                                        _b = ['log remote invocation failed', r.status, r.statusText];
                                        return [4 /*yield*/, r.text()];
                                    case 1:
                                        _a.apply(void 0, [_b.concat([_c.sent()]), 'error', { remote: false }]);
                                        _c.label = 2;
                                    case 2: return [2 /*return*/];
                                }
                            });
                        }); }).catch(function (e) {
                            log(['log remote invocation failed', e], 'error', { remote: false });
                        })];
                case 1:
                    _a.sent();
                    _a.label = 2;
                case 2: return [2 /*return*/];
            }
        });
    });
}
exports.log = log;
function objectifyError(e) {
    if (e instanceof Error) {
        return __assign({}, e, { message: e.message, name: e.name, stack: e.stack });
    }
    return e;
}
