import 'log-timestamp';
export declare function log(message: any | any[], level?: LogLevel, opts?: LogOptions): Promise<void>;
export declare type LogLevel = 'info' | 'debug' | 'warning' | 'error';
export interface LogOptions {
    /**
     * Whether to send to the remote API.
     */
    remote?: boolean;
}
